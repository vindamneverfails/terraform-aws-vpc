# VPC
output "vpc_id" {
  description = "The ID of the VPC"
  value       = module.vpc.vpc_id
}

# Subnets
output "private_subnets" {
  description = "List of IDs of private subnets"
  value       = module.vpc.private_subnets
}

output "public_subnets" {
  description = "List of IDs of public subnets"
  value       = module.vpc.public_subnets
}

output "database_subnets" {
  description = "List of IDs of database subnets"
  value       = module.vpc.database_subnets
}

output "elasticache_subnets" {
  description = "List of IDs of elasticache subnets"
  value       = module.vpc.elasticache_subnets
}

output "redshift_subnets" {
  description = "List of IDs of redshift subnets"
  value       = module.vpc.redshift_subnets
}

output "intra_subnets" {
  description = "List of IDs of intra subnets"
  value       = module.vpc.intra_subnets
}

# NAT gateways
output "nat_public_ips" {
  description = "List of public Elastic IPs created for AWS NAT Gateway"
  value       = module.vpc.nat_public_ips
}

# Customer Gateway
output "cgw_ids" {
  description = "List of IDs of Customer Gateway"
  value       = module.vpc.cgw_ids
}

output "this_customer_gateway" {
  description = "Map of Customer Gateway attributes"
  value       = module.vpc.this_customer_gateway
}
# Master
output "master_db_instance_address" {
  description = "The address of the RDS instance"
  value       = module.master.db_instance_address
}

output "master_db_instance_arn" {
  description = "The ARN of the RDS instance"
  value       = module.master.db_instance_arn
}

output "master_db_instance_availability_zone" {
  description = "The availability zone of the RDS instance"
  value       = module.master.db_instance_availability_zone
}

output "master_db_instance_endpoint" {
  description = "The connection endpoint"
  value       = module.master.db_instance_endpoint
}

output "master_db_instance_hosted_zone_id" {
  description = "The canonical hosted zone ID of the DB instance (to be used in a Route 53 Alias record)"
  value       = module.master.db_instance_hosted_zone_id
}

output "master_db_instance_id" {
  description = "The RDS instance ID"
  value       = module.master.db_instance_id
}

output "master_db_instance_resource_id" {
  description = "The RDS Resource ID of this instance"
  value       = module.master.db_instance_resource_id
}

output "master_db_instance_status" {
  description = "The RDS instance status"
  value       = module.master.db_instance_status
}

output "master_db_instance_name" {
  description = "The database name"
  value       = module.master.db_instance_name
}

output "master_db_instance_username" {
  description = "The master username for the database"
  value       = module.master.db_instance_username
  sensitive   = true
}

output "master_db_instance_password" {
  description = "The database password (this password may be old, because Terraform doesn't track it after initial creation)"
  value       = module.master.db_instance_password
  sensitive   = true
}

output "master_db_instance_port" {
  description = "The database port"
  value       = module.master.db_instance_port
}

output "master_db_subnet_group_id" {
  description = "The db subnet group name"
  value       = module.master.db_subnet_group_id
}

output "master_db_subnet_group_arn" {
  description = "The ARN of the db subnet group"
  value       = module.master.db_subnet_group_arn
}

# Replica
output "replica_db_instance_address" {
  description = "The address of the RDS instance"
  value       = module.replica.db_instance_address
}

output "replica_db_instance_arn" {
  description = "The ARN of the RDS instance"
  value       = module.replica.db_instance_arn
}

output "replica_db_instance_availability_zone" {
  description = "The availability zone of the RDS instance"
  value       = module.replica.db_instance_availability_zone
}

output "replica_db_instance_endpoint" {
  description = "The connection endpoint"
  value       = module.replica.db_instance_endpoint
}

output "replica_db_instance_hosted_zone_id" {
  description = "The canonical hosted zone ID of the DB instance (to be used in a Route 53 Alias record)"
  value       = module.replica.db_instance_hosted_zone_id
}

output "replica_db_instance_id" {
  description = "The RDS instance ID"
  value       = module.replica.db_instance_id
}

output "replica_db_instance_resource_id" {
  description = "The RDS Resource ID of this instance"
  value       = module.replica.db_instance_resource_id
}

output "replica_db_instance_status" {
  description = "The RDS instance status"
  value       = module.replica.db_instance_status
}

output "replica_db_instance_name" {
  description = "The database name"
  value       = module.replica.db_instance_name
}

output "replica_db_instance_username" {
  description = "The replica username for the database"
  value       = module.replica.db_instance_username
  sensitive   = true
}

output "replica_db_instance_port" {
  description = "The database port"
  value       = module.replica.db_instance_port
}
output "vpc_cidr" {
  value       = module.vpc.vpc_cidr_block
  description = "VPC CIDR"
}

output "redis_cluster_id" {
  value       = module.redis.id
  description = "Redis cluster ID"
}

output "redis_cluster_endpoint" {
  value       = module.redis.endpoint
  description = "Redis primary endpoint"
}

output "redis_cluster_host" {
  value       = module.redis.host
  description = "Redis hostname"
}

output "redis_cluster_security_group_id" {
  value       = module.redis.security_group_id
  description = "Redis Security Group ID"
}

output "redis_cluster_security_group_arn" {
  value       = module.redis.security_group_arn
  description = "Redis Security Group ARN"
}

output "redis_cluster_security_group_name" {
  value       = module.redis.security_group_name
  description = "Redis Security Group name"
}
################################################################################
# Complete
################################################################################

# Launch template
output "complete_lt_launch_template_id" {
  description = "The ID of the launch template"
  value       = module.complete_lt.launch_template_id
}

output "complete_lt_launch_template_arn" {
  description = "The ARN of the launch template"
  value       = module.complete_lt.launch_template_arn
}

output "complete_lt_launch_template_latest_version" {
  description = "The latest version of the launch template"
  value       = module.complete_lt.launch_template_latest_version
}

output "complete_lt_autoscaling_group_id" {
  description = "The autoscaling group id"
  value       = module.complete_lt.autoscaling_group_id
}

output "complete_lt_autoscaling_group_name" {
  description = "The autoscaling group name"
  value       = module.complete_lt.autoscaling_group_name
}

output "complete_lt_autoscaling_group_arn" {
  description = "The ARN for this AutoScaling Group"
  value       = module.complete_lt.autoscaling_group_arn
}

output "complete_lt_autoscaling_group_min_size" {
  description = "The minimum size of the autoscale group"
  value       = module.complete_lt.autoscaling_group_min_size
}

output "complete_lt_autoscaling_group_max_size" {
  description = "The maximum size of the autoscale group"
  value       = module.complete_lt.autoscaling_group_max_size
}

output "complete_lt_autoscaling_group_desired_capacity" {
  description = "The number of Amazon EC2 instances that should be running in the group"
  value       = module.complete_lt.autoscaling_group_desired_capacity
}

output "complete_lt_autoscaling_group_default_cooldown" {
  description = "Time between a scaling activity and the succeeding scaling activity"
  value       = module.complete_lt.autoscaling_group_default_cooldown
}

output "complete_lt_autoscaling_group_health_check_grace_period" {
  description = "Time after instance comes into service before checking health"
  value       = module.complete_lt.autoscaling_group_health_check_grace_period
}

output "complete_lt_autoscaling_group_health_check_type" {
  description = "EC2 or ELB. Controls how health checking is done"
  value       = module.complete_lt.autoscaling_group_health_check_type
}

output "complete_lt_autoscaling_group_availability_zones" {
  description = "The availability zones of the autoscale group"
  value       = module.complete_lt.autoscaling_group_availability_zones
}

output "complete_lt_autoscaling_group_vpc_zone_identifier" {
  description = "The VPC zone identifier"
  value       = module.complete_lt.autoscaling_group_vpc_zone_identifier
}

output "complete_lt_autoscaling_group_load_balancers" {
  description = "The load balancer names associated with the autoscaling group"
  value       = module.complete_lt.autoscaling_group_load_balancers
}

output "complete_lt_autoscaling_group_target_group_arns" {
  description = "List of Target Group ARNs that apply to this AutoScaling Group"
  value       = module.complete_lt.autoscaling_group_target_group_arns
}

output "complete_lt_autoscaling_schedule_arns" {
  description = "ARNs of autoscaling group schedules"
  value       = module.complete_lt.autoscaling_schedule_arns
}
